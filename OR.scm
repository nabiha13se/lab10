; or function



;OR := ?p.?q.p p p

;(define (and p q)(p p q))


;TRUE := ?x.?y.x

;FALSE := ?x.?y.y


(define (true x y)(x))

(define (false x y)(y))


;lamda mn.lamda ab.n a(m a b)

(define (or m n)(((define (a)())(define (b)())(n a(m a b)))))


(or false true )
