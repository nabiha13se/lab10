; LT function


(define (four s z) (s s s s z))


(define (three s z) (s s s z))



(define (true x y)(x))


(define (false x y)(y))


;not function

(define (not n)(((define (a)())(define (b)())(n b a))))


;iszero function

(define (Iszero n)(n (define (x)())(false)true)
	

;predecessor function

(define (pred n f x)(n(define (g h)(h (g f))(define (u)(x))(define (u)(u)))


 ;subtract := lamda mn.n pred m  
 
(define (subtract n m)(n pred m))

;and function

(define (and m n)(((define (a)())(define (b)())(n(m a b)b))))

;LEQ function

(define (LEQ m n)(Iszero(subtract m n)))

;EQ function

(define (EQ m n)(and (LEQ m n)(LEQ n m)))

;LT function

(define (LT m n)(and (not(EQ m n))(LEQ m n))

;GEQ function

(define (GEQ m n)((not(LT m n))))
	

(GEQ four three)