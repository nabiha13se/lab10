; AND function



;AND := ?p.?q.p q p

;(define (and p q)(p q p))


;TRUE := ?x.?y.x

;FALSE := ?x.?y.y


(define (true x y)(x))

(define (false x y)(y))


;lamda mn.lamda ab.n(m a b)b

(define (and m n)(((define (a)())(define (b)())(n(m a b)b))))


(and true false )
